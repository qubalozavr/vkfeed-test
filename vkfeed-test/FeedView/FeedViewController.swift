//
//  FeedViewController.swift
//  vkfeed-test
//
//  Created by Валентин Казанцев on 12.07.2020.
//  Copyright © 2020 Валентин Казанцев. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBlue
    }
}
