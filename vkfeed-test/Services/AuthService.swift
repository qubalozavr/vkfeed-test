//
//  AuthService.swift
//  vkfeed-test
//
//  Created by Валентин Казанцев on 11.07.2020.
//  Copyright © 2020 Валентин Казанцев. All rights reserved.
//

import Foundation
import VK_ios_sdk

protocol AuthServiceDelegate: class {
    func authServiceShouldShow(viewController: UIViewController)
    func authServiceSignIn()
    func authServiceSignInDidFail()
}

class AuthService: NSObject, VKSdkDelegate, VKSdkUIDelegate {

    private let appId = "7535621"
    private let vkSDK: VKSdk
    weak var delegate: AuthServiceDelegate?

    override init() {
        vkSDK = VKSdk.initialize(withAppId: appId)
        super.init()
        print("INITIALIZED") //TODO: clean this shit up
        vkSDK.register(self)
        vkSDK.uiDelegate = self
    }

    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        print(#function)
        if result.token != nil {

        }

    }

    func vkSdkUserAuthorizationFailed() {
        print(#function)
        delegate?.authServiceSignInDidFail()
    }

    func vkSdkShouldPresent(_ controller: UIViewController!) {
        print(#function)
        delegate?.authServiceShouldShow(viewController: controller)
    }

    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        print(#function)
    }

    func wakeUpSession() {
        let scope = ["offline"]
        VKSdk.wakeUpSession(scope) { [delegate](state, error) in
            switch state {
            case .initialized:
                print("INITIALIZED")
                VKSdk.authorize(scope)
            case .authorized:
                print("AUTHORIZED")
                delegate?.authServiceSignIn()
            case .error:
                fatalError(error?.localizedDescription as! String)
            @unknown default:
                delegate?.authServiceSignInDidFail()
            }
        }
    }

}
