//
//  ViewController.swift
//  vkfeed-test
//
//  Created by Валентин Казанцев on 10.07.2020.
//  Copyright © 2020 Валентин Казанцев. All rights reserved.
//

import UIKit
import VK_ios_sdk

class AuthViewController: UIViewController {

    private var authService: AuthService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        authService = SceneDelegate.shared().authService 
    }
    
    override var shouldAutorotate: Bool{
        return false
    }


    @IBAction func signInTouch(_ sender: UIButton) {
        authService.wakeUpSession()
    }
}

